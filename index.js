window.addEventListener('load', _ => {
  const webglCanvas = document.querySelector('#webglCanvas');

  const context = webglCanvas.getContext('webgl');
  if (context === null) {
    throw new Error('WebGL is not available.');
  }

  context.clearColor(0, 0, 0, 1);
  context.clear(context.COLOR_BUFFER_BIT);

  const vertexShader = context.createShader(context.VERTEX_SHADER);
  context.shaderSource(vertexShader, `
    attribute vec4 aVertexPosition;
    
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;
    
    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    }
  `);

  context.compileShader(vertexShader);
  console.log(context.getShaderParameter(vertexShader, context.COMPILE_STATUS));

  const fragmentShader = context.createShader(context.FRAGMENT_SHADER);
  context.shaderSource(fragmentShader, `
    void main() {
      gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
  `);

  context.compileShader(fragmentShader);
  console.log(context.getShaderParameter(fragmentShader, context.COMPILE_STATUS));

  const shaderProgram = context.createProgram();
  context.attachShader(shaderProgram, vertexShader);
  context.attachShader(shaderProgram, fragmentShader);
  context.linkProgram(shaderProgram);
  console.log(context.getProgramParameter(shaderProgram, context.LINK_STATUS));

  // TODO: Continue with https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Adding_2D_content_to_a_WebGL_context
});
